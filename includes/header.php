<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">

    
    <!-- Ionicons-->
    <link rel="stylesheet" href="vendor/ionicons/css/ionicons.min.css">


 

     <!-- fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- light-box -->
    <link href="vendor/lightbox/dist/css/lightbox.css" rel="stylesheet">

    <!-- Slick CSS-->
    <link rel="stylesheet" href="vendor/slick/slick/slick.css">
    <link rel="stylesheet" href="vendor/slick/slick/slick-theme.css">

       <!-- Custom CSS-->
       <link rel="stylesheet" href="assets/scss/master.css">

  
    <title>Paracosma tv</title>
  </head>
  <body>