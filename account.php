<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>

<section class="breadcrumb-wrapper">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">User Account</li>
            </ol>
        </nav>
    </div>
</section>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <?php include 'includes/side-tabs.php';?>
            </div>
            <div class="col-md-10">
                <div class="profile-wrapper">
                    <div class="row">
                        <div class="col-md-3 text-right">
                            <div class="user-image">
                                <figure>
                                    <img src="assets/images/author/auth-ashish.jpg" alt="" class="auth-img">
                                </figure>
                                <a href="" data-toggle="modal" data-target="#costumModalChangeAvatar">Change Avatar</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="user-details">
                                <h4> Ashish Dhakal</h4>
                                <ul class="list-unstyled mail-loc">
                                    <li><span><img src="assets/svg/user-icon.svg" alt=""></span> dhakal@paracosma.com</li>
                                    <li><span><img src="assets/svg/location-icon.svg" alt=""></span> Lives In kathmandu , Nepal</li>
                                </ul>
                                <p>I am a passionate photographer always looking for new things to discover around us and analyze to style them through photograph Lorem ipsum dolor sit amet consectetur, adipisicing elit. Modi hic a iure repudiandae expedita id nulla veritatis, provident distinctio inventore consequatur animi vitae esse pariatur maxime, impedit sunt delectus! Modi.</p>
                                <ul class="list-inline user-action">
                                    <li><span>22</span> Images</li>
                                    <li><span>32</span> Videos</li>
                                    <li class="pull-right"><a href=""><span><img src="assets/svg/exit.svg" alt=""></span>Sign Out</a></li>
                                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#costumModalEditProfile"><span><img src="assets/svg/settings.svg" alt=""></span>Edit Profile</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-wrapper">
                    <div class="featured-content">
                        <div class="content-headline">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4> Private Uploaded Media</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="active"><a data-toggle="tab" href="#all">All</a></li>
                                        <li><a data-toggle="tab" href="#images">360 Images</a></li>
                                        <li><a data-toggle="tab" href="#videos">360 Videos</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">
                                <div id="all" class="tab-pane fade in active">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="images" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="videos" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                

                                
                            </div>
                        </div>
                    </div>
                    <div class="featured-content">
                        <div class="content-headline">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4> Public Uploaded Media</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="active"><a data-toggle="tab" href="#p-all">All</a></li>
                                        <li><a data-toggle="tab" href="#p-images">360 Images</a></li>
                                        <li><a data-toggle="tab" href="#p-videos">360 Videos</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">
                                <div id="p-all" class="tab-pane fade in active">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="p-images" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="p-videos" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1`.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail auth-control">
                                                                <ul class="list-inline">
                                                                    <li class=""><a href="edit-media.php">Edit</a></li>
                                                                    <li class=""><a href="">Delete</a></li>
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<?php include 'includes/footer.php';?>
<?php include 'includes/modals.php';?>
