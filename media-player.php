<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
<section class="breadcrumb-wrapper">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Media Library</li>
            </ol>
        </nav>
    </div>
</section>
<section class="main-content insider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <?php include 'includes/side-tabs.php';?>
            </div>
            <div class="col-md-8">
                <div class="media-player-wrapper">
                    <div class="media-player-content">
                        <figure>
                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                        </figure>
                    </div>
                    <div class="media-player-details">
                        <div class="details-tags">
                            <ul class="list-inline">
                                <li class="views"> 360 Views</li>
                                <li class="tag">#Travel&Tours</li>
                            </ul>
                        </div>
                        <div class="details-title">
                            <div class="row">
                                <div class="col-md-6">  <h2>Sunset with berry of bim</h4> </div>
                                <div class="col-md-6 text-right"> 
                                    <a class="btn-show" data-toggle="collapse" href="#collapseImage" role="button" aria-expanded="false" aria-controls="collapseImage"> 
                                        <span class="" ></span> Show More Images
                                    </a>
                                </div>
                            </div>
                            <div class="extra-image">
                                <div class="collapse" id="collapseImage">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <figure>
                                                    <a href="assets/images/travel-1.jpg" data-lightbox="light"><img src="assets/images/travel-1.jpg" alt="" class="img-responsive"></a>
                                                </figure>
                                            </div>
                                            <div class="col-md-3">
                                                <figure>
                                                    <a href="assets/images/travel-1.jpg" data-lightbox="light"><img src="assets/images/travel-1.jpg" alt="" class="img-responsive"></a>
                                                </figure>
                                            </div>
                                            <div class="col-md-3">
                                                <figure>
                                                    <a href="assets/images/travel-1.jpg" data-lightbox="light"><img src="assets/images/travel-1.jpg" alt="" class="img-responsive"></a>
                                                </figure>
                                            </div>
                                            <div class="col-md-3">
                                                <figure>
                                                    <a href="assets/images/travel-1.jpg" data-lightbox="light"><img src="assets/images/travel-1.jpg" alt="" class="img-responsive"></a>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="media-auth-details">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="auth-details-wrapper">
                                        <ul class="list-inline">
                                            <li class="auth-image-list">
                                                <a href="profile.php">
                                                <img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive">
                                                </a>
                                            </li>
                                            <li class="auth-name-list">
                                                <strong>By</strong><br>
                                                <span>Nikki James</span><br>
                                                <small>Uploaded 20 Weeks Ago</small>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="media-desc-wrapper">
                                        <h4>Media Description</h4>
                                        <p>Cras gravida bibendum dolor eu varius. Morbi fermentum velit nisl, eget vehicula lorem sodales eget. Donec quis volutpat orci. Sed ipsum felis, tristique id egestas et, convallis ac velit. In consequat dolor libero, nec luctus orci rutrum nec. Phasellus vel arcu sed nibh ornare accumsan. Vestibulum in elementum erat. </p>
                                    </div>
                                    <div class="media-comments-wrapper">
                                        <h4>11 Comments</h4>
                                        <form action="">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href=""><img src="assets/images/author/auth-ashish.jpg" alt="" class="auth-img"></a>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <textarea name="" id="" cols="30" rows="5" class="form-control">Write Comment here...</textarea>
                                                        <label for="">Commenting as Ashish Dhakal</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <button class="btn btn-black">Post Comment</button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="com-list">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href=""><img src="assets/images/author/auth-3.jpg" alt="" class="auth-img"></a>
                                                </div>
                                                <div class="col-md-8">
                                                    <ul class="list-unstyled">
                                                        <li><span>Angelina Wines</span></li>
                                                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi repudiandae iure non vel quidem unde aliquam voluptas tempore cumque quaerat soluta, alias distinctio doloremque hic explicabo molestias, delectus ex expedita?</li>
                                                        <li><small>1 day ago</small></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="com-list">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href=""><img src="assets/images/author/auth-6.jpg" alt="" class="auth-img"></a>
                                                </div>
                                                <div class="col-md-8">
                                                    <ul class="list-unstyled">
                                                        <li><span>Tom Clay</span></li>
                                                        <li>Nisi repudiandae iure non vel quidem unde aliquam voluptas tempore cumque quaerat soluta, alias distinctio doloremque hic explicabo molestias, delectus ex expedita? Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem obcaecati illo porro odit eaque quasi consequatur ullam eum magnam corrupti, rem ex reprehenderit voluptas! Esse totam provident est inventore rem?</li>
                                                        <li><small>2 weeks ago</small></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="com-list">
                                            <div class="row">
                                                <div class="col-md-1 text-center">
                                                    <a href=""><img src="assets/images/author/auth-4.jpg" alt="" class="auth-img"></a>
                                                </div>
                                                <div class="col-md-8">
                                                    <ul class="list-unstyled">
                                                        <li><span>Cathrine Dilop</span></li>
                                                        <li>alias distinctio doloremque hic explicabo molestias, delectus ex expedita? Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ullam atque officia maiores fugiat expedita rem nesciunt omnis error sequi nobis ipsum, rerum, modi quote.</li>
                                                        <li><small> 2 Weeks ago</small></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="load-more">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <a href="" class="btn btn-load">Load More Comments</a>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="text-right share">
                                        <a href=""><img src="assets/svg/fb-share.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="content-wrapper">
                    <div class="content-headline">
                        <h4> More From Uploader</h4>
                    </div>
                    <div class="content-body uploader">
                        <div class="box">
                            <div class="content-box">
                                <figure>
                                    <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                </figure>
                                <div class="content-media">
                                    <h4>Sunset with the berry of bim</h4>
                                <div class="media-detail">
                                    <ul class="list-inline">
                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                        <li>Nikki</li>
                                        <li class="border-pane"> 10 weeks ago</li>
                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                    </ul>
                                </div>
                                <div class="media-ancor">
                                    <a href=""></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="box">
                            <div class="content-box">
                                <figure>
                                    <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                </figure>
                                <div class="content-media">
                                    <h4>Sunset with the berry of bim</h4>
                                <div class="media-detail">
                                    <ul class="list-inline">
                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                        <li>Nikki</li>
                                        <li class="border-pane"> 10 weeks ago</li>
                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                    </ul>
                                </div>
                                <div class="media-ancor">
                                    <a href=""></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="box">
                            <div class="content-box">
                                <figure>
                                    <img src="assets/images/15.jpg" alt="" class="img-responsive">
                                </figure>
                                <div class="content-media">
                                    <h4>Sunset with the berry of bim</h4>
                                <div class="media-detail">
                                    <ul class="list-inline">
                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                        <li>Nikki</li>
                                        <li class="border-pane"> 10 weeks ago</li>
                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                    </ul>
                                </div>
                                <div class="media-ancor">
                                    <a href=""></a>
                                </div>
                            </div>
                            
                        </div>

                     </div>
                    </div>
                    <div class="load-more">
                        <a href="" class="btn btn-load">See More From Uploader </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="main-content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div class="content-wrapper">
            <div class="featured-content">
                        <div class="content-headline">
                            <h4> Similar Uploaded Media</h4>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">                               
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-3.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-4.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-5.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-6.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
    
</section>


<!-- // facebook plugin -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- // End of facebook plugin -->

<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
</script>

<?php include 'includes/modals.php';?>
<?php include 'includes/footer.php';?>

