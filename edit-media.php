<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
<section class="breadcrumb-wrapper">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Media Library</li>
            </ol>
        </nav>
    </div>
</section>
<section class="main-content insider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <?php include 'includes/side-tabs.php';?>
            </div>
            <div class="col-md-8">
                <div class="auth-upload-wrapper">
                    <ul class="list-inline">
                        <li><span><img src="assets/images/author/auth-ashish.jpg" alt="" class="auth-img"></span></li>
                        <li><strong>Edit This Media</strong></li>
                        <li class="pull-right"><a href="account.php">Go to account</a></li>
                    </ul>
                    <div class="upload-wrapper">
                        <div class="row">
                            <div class="col-md-8 bg-upload">
                            <div class="dropzone-wrapper">
                                <h4><strong>Edit main file for media</strong></h4>
                                        <form action="upload" class="dropzone" id="my-awesome-dropzone">
                                            <div class="dz-message" data-dz-message> 
                                                <figure>
                                                    <img src="assets/svg/file-icon.svg" alt="" class="">
                                                <figure>
                                                <span>Drag your file over here to upload</span>
                                                <p><small>Supported file type : JPEG , PNG , MKV , MP4</small></p>
                                            </div>
                                            <div class="fallback">
                                            <input name="file" type="file" single /> 
                                            </div>
                                        </form>      
                            </div>
                            <div class="dropzone-wrapper plain-dropzone-wrapper">
                                <h4><strong>Edit plain images ( If Any )</strong></h4>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <form action="upload" class="dropzone" id="my-awesome-dropzone">
                                            <div class="dz-message" data-dz-message> 
                                                <figure>
                                                    <img src="assets/svg/file-icon.svg" alt="" class="">
                                                <figure>
                                            </div>
                                            <div class="fallback">
                                            <input name="file" type="file" single /> 
                                            </div>
                                            <a href="">Remove</a>
                                        </form>
                                        </div>
                                        <div class="col-md-3">
                                        <form action="upload" class="dropzone" id="my-awesome-dropzone">
                                            <div class="dz-message" data-dz-message> 
                                                <figure>
                                                    <img src="assets/svg/file-icon.svg" alt="" class="">
                                                <figure>
                                            </div>
                                            <div class="fallback">
                                            <input name="file" type="file" single /> 
                                            </div>
                                            <a href="">Remove</a>
                                        </form>
                                        </div>
                                        <div class="col-md-3">
                                        <form action="upload" class="dropzone" id="my-awesome-dropzone">
                                            <div class="dz-message" data-dz-message> 
                                                <figure>
                                                    <img src="assets/svg/file-icon.svg" alt="" class="">
                                                <figure>
                                            </div>
                                            <div class="fallback">
                                            <input name="file" type="file" single /> 
                                            </div>
                                            <a href="">Remove</a>
                                        </form>
                                        </div>
                                        <div class="col-md-3">
                                        <form action="upload" class="dropzone" id="my-awesome-dropzone">
                                            <div class="dz-message" data-dz-message> 
                                                <figure>
                                                    <img src="assets/svg/file-icon.svg" alt="" class="">
                                                <figure>
                                            </div>
                                            <div class="fallback">
                                            <input name="file" type="file" single /> 
                                            </div>
                                            <a href="">Remove</a>
                                        </form>
                                        </div>
                                    </div>
                                              
                            </div>
                            </div>
                            <div class="col-md-4 bg-action">
                                <div class="alert alert-primary alert-dismissible fade collapse" role="alert" id="collapseEdit">
                                    <strong> Media Updated !</strong> You sucessfully uploded new media file.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <ul class="nav nav-tabs custom-navs">
                                    <li class="active"><a data-toggle="tab" href="#opt">Options</a></li>
                                    <li><a data-toggle="tab" href="#des">Description</a></li>          
                                    <li><a data-toggle="tab" href="#sub">Submit</a></li>          
                                </ul> 
                                <div class="tab-content">
                                        <div id="opt" class="tab-pane fade in active">
                                        <form action="" class="form-wrapper">
                                            <div class="form-group">
                                                <label for="">Edit Your File Type</label>
                                                <select class="form-control" id="">
                                                    <option>360 Image</option>
                                                    <option>360 Video</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class=""> Edit Media Privacy Type </label>
                                                <div class="radio-wrapper">
                                                    <label class="radio-inline">
                                                                <input type="radio" name="optradio" checked> <span>Make It Public</span> 
                                                                </label>
                                                                <label class="radio-inline">
                                                                <input type="radio" name="optradio"><span>Make It Private</span> 
                                                                </label>            
                                                </div>    
                                                <small>This will allow your resource to be shown or not to be shown to other user You can change visibility option later also ,  by default it will be public</small>  
                                            </div>
                                            <div class="form-group">
                                                <label for="">Edit Your Media Category</label>
                                                <select class="form-control" id="">
                                                    <option>Travel & Tours</option>
                                                    <option>Foods</option>
                                                    <option>Culture & Arts</option>
                                                    <option>Infrastructure</option>
                                                    <option>Fashions</option>
                                                    <option>Sports & Action</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <a data-toggle="tab" href="#des" class="btn btn-black"> Save & Continue</a>
                                            </div>
                                            </form>
                                        </div>
                                        <div id="des" class="tab-pane fade in">
                                            <form action="" class="form-wrapper">
                                                <div class="form-group">
                                                    <label for="">Edit Media Title</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Edit Media Description</label>
                                                    <textarea name="" id="" cols="30" rows="8" class="form-control"> Enter text here...</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <a data-toggle="tab" href="#sub" class="btn btn-black"> Save & Continue</a>
                                                    <a data-toggle="tab" href="#opt" class="btn btn-load" > <span class="ion-arrow-left-c"></span> Go back </a>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="sub" class="tab-pane fade in">
                                            <form action="" class="form-wrapper">
                                                <div class="form-group">
                                                    <div class="well well-lg well-p">
                                                        <ul class="list-unstyled summary">
                                                            <li><span class="ion-android-checkmark"></span> Media file Edited <strong>(53.7 MB)</strong></li>
                                                            <li><span class="ion-android-checkmark"></span> Plain Image Edited <strong>(No of uploads : 3)</strong></li>
                                                            <li><span class="ion-android-checkmark"></span> File Type Edited <strong>( 360 Image)</strong></li>
                                                            <li><span class="ion-android-checkmark"></span> Privacy <strong>( Unchanged)</strong></li>
                                                            <li><span class="ion-android-checkmark"></span> Category  <strong>( Unchanged )</strong></li>
                                                            <li><span class="ion-android-checkmark"></span> Description Edited</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <a data-toggle="collapse" href="#collapseEdit" role="button" aria-expanded="false" aria-controls="collapseExample" class="btn btn-black"> Submit Upload</a>
                                                    <a data-toggle="tab" href="#des" class="btn btn-load" > <span class="ion-arrow-left-c"></span> Go back </a>
                                                </div>    
                                            </form>   
                                        </div>
                                    
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="content-wrapper">
                    <div class="content-headline">
                        <h4> Hot Uploads This Week</h4>
                    </div>
                    <div class="content-body uploader">
                        <div class="box">
                            <div class="content-box">
                                <figure>
                                    <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                </figure>
                                <div class="content-media">
                                    <h4>Sunset with the berry of bim</h4>
                                <div class="media-detail">
                                    <ul class="list-inline">
                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                        <li>Nikki</li>
                                        <li class="border-pane"> 10 weeks ago</li>
                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                    </ul>
                                </div>
                                <div class="media-ancor">
                                    <a href=""></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="box">
                            <div class="content-box">
                                <figure>
                                    <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                </figure>
                                <div class="content-media">
                                    <h4>Sunset with the berry of bim</h4>
                                <div class="media-detail">
                                    <ul class="list-inline">
                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                        <li>Nikki</li>
                                        <li class="border-pane"> 10 weeks ago</li>
                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                    </ul>
                                </div>
                                <div class="media-ancor">
                                    <a href=""></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="box">
                            <div class="content-box">
                                <figure>
                                    <img src="assets/images/15.jpg" alt="" class="img-responsive">
                                </figure>
                                <div class="content-media">
                                    <h4>Sunset with the berry of bim</h4>
                                <div class="media-detail">
                                    <ul class="list-inline">
                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                        <li>Nikki</li>
                                        <li class="border-pane"> 10 weeks ago</li>
                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                    </ul>
                                </div>
                                <div class="media-ancor">
                                    <a href=""></a>
                                </div>
                            </div>
                            
                        </div>

                     </div>
                    </div>
                    <div class="load-more">
                        <a href="" class="btn btn-load">See More From Uploader </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



<!-- // facebook plugin -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- // End of facebook plugin -->



<?php include 'includes/footer.php';?>
<?php include 'includes/modals.php';?>
