<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>

<section class="breadcrumb-wrapper">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Travel & Tours</li>
            </ol>
        </nav>
    </div>
</section>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <?php include 'includes/side-tabs.php';?>
            </div>
            <div class="col-md-10">
                <div class="profile-wrapper">
                    <div class="row">
                        <div class="col-md-3 text-right">
                            <div class="user-image">
                                <figure>
                                    <img src="assets/images/travel-avatar.jpeg" alt="" class="auth-img">
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="user-details">
                                <h4>Travel & Tours</h4>
                                <ul class="list-inline mail-loc tags-point">
                                    <li> #travel</li>
                                    <li> #tours</li>
                                    <li> #safari</li>
                                    <li> #adventure</li>
                                </ul>
                                <p>I am a passionate photographer always looking for new things to discover around us and analyze to style them through photograph Lorem ipsum dolor sit amet consectetur, adipisicing elit. Modi hic a iure repudiandae expedita id nulla veritatis, provident distinctio inventore consequatur animi vitae esse pariatur maxime, impedit sunt delectus! Modi.</p>
                                <ul class="list-inline user-action">
                                    <li><span>2</span> Images</li>
                                    <li><span>8</span> Videos</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-wrapper">
                    <div class="featured-content">
                        <div class="content-headline">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4> Travel & Tours</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="active"><a data-toggle="tab" href="#all">All</a></li>
                                        <li><a data-toggle="tab" href="#images">360 Images</a></li>
                                        <li><a data-toggle="tab" href="#videos">360 Videos</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">
                                <div id="all" class="tab-pane fade in active">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div id="images" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="videos" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/11.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/8.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/15.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href=""></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/10.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/9.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<?php include 'includes/footer.php';?>
<?php include 'includes/modals.php';?>
