 <footer class="footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <p>Paracosma Tv is a project by Paracosma © 2016-20190 Paracosma Inc ,  All rights reserved.</p>
        </div>
        <div class="col-md-4">
            <div class="social-links">
                <ul class="list-inline">
                  <li><a href=""><img src="assets/svg/instagram.svg" alt=""></a></li>
                  <li><a href=""><img src="assets/svg/facebook.svg" alt=""></a></li>
                  <li><a href=""><img src="assets/svg/youtube-icon.svg" alt=""></a></li>
                </ul>
            </div>
        </div>
      </div>
    </div>
 </footer>



 <!-- jQuery first -->
 <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <!-- lightbox -->
  <script src="vendor/lightbox/dist/js/lightbox-plus-jquery.js"></script>
   

    <!-- bootstrap -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
     <!-- slick slider-->
     <script src="vendor/slick/slick/slick.min.js"></script> 
     

     <!-- Dropzone -->
     <script src="vendor/dropzone/dropzone.js"></script>

     

    <!-- custom javascript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="assets/js/custom.js"></script>
   
    
    
  </body>
</html>

