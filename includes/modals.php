<!-- Modal Search -->
<div id="costumModalSearch" class="modal modal-search" data-easein="slideUpBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                            <form action="" class="form-wrapper">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Search Here ..." autofocus >
                                </div>
                            </form>
                    </div>
                    <div class="modal-body">
                        <div class="heading-text">
                            <h3><strong>15 </strong> Search Result Found</h3>
                        </div>
                        <div class="modal-search-slider">
                            <div class="modal-slide">
                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                </div>
                            </div>
                            <div class="modal-slide">
                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                </div>
                            </div>
                            <div class="modal-slide">
                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                </div>
                            </div>
                            <div class="modal-slide">
                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- Modal Sign In -->
        <div id="costumModalSignIn" class="modal modal-auth" data-easein="slideLeftBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                                <div class="heading-text">
                                    <h3><strong>Sign In</strong> To Your Account</h3>
                                    <p>I still dont have my account yet ! <a data-toggle="modal" data-target="#costumModalSignUp" role="button" data-dismiss="modal">Sign Up Now</a></p>
                                </div>
                            </div>
                            <div class="modal-body">
                            <form action="" class="form-wrapper">
                                        <div class="form-group">
                                            <label for="">Enter Your Email Address</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <ul class="list-inline">
                                                <li><label for="">Enter Your password</label></li>
                                                <li class="pull-right"><label for=""><a href="">Forgot Password ? </a></label></li>
                                            </ul>
                                            <input type="password" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <a href="account.php" class="btn btn-black">Sign In</a>
                                        </div>
                            </form>
                            <div class="text-center">
                                OR
                            </div>
                            <div class="facebook-login">
                                <a href="" class="btn-fb"> Sign In With Facebook </a>
                            </div>
                            </div>
                </div>
                
            </div>
        </div>


        <!-- Modal Sign Up -->
        <div id="costumModalSignUp" class="modal modal-auth" data-easein="slideRightBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                                <div class="heading-text">
                                    <h3><strong>Sign Up</strong> To Your Account</h3>
                                    <p>I already have my account yet ! <a data-toggle="modal" data-target="#costumModalSignIn" role="button" data-dismiss="modal">Sign In Now</a></p>
                                </div>
                            </div>
                            <div class="modal-body">
                            <form action="" class="form-wrapper">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Enter First name</label>
                                            <input type="text" class="form-control" placeholder="" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="">Enter last name</label>
                                            <input type="text" class="form-control" placeholder="" autofocus>
                                        </div>
                                    </div>
                                </div>
                                        <div class="form-group">
                                            <label for="">Enter Your Email Address</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Create Your password</label>
                                            <input type="password" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Re-enter Your password</label>
                                            <input type="password" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-black">Create New Account</button>
                                        </div>
                            </form>
                            
                            </div>
                </div>
                
            </div>
        </div>
         

        <div id="costumModalChangeAvatar" class="modal modal-auth" data-easein="slideUpBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                                <div class="heading-text">
                                    <h3><strong>Change </strong> Your Avatar</h3>
                                    <p>The image you will upload will appear as your new profile image</p>
                                </div>
                            </div>
                            <div class="modal-body">
                            <div class="dropzone-wrapper">
                                <form action="upload" class="dropzone" id="my-awesome-dropzone">
                                                <div class="dz-message" data-dz-message> 
                                                    <figure>
                                                        <img src="assets/svg/file-icon.svg" alt="" class="">
                                                    <figure>
                                                    <span>Drag your file over here to upload</span>
                                                    <p><small>Supported file type : JPEG , PNG </small></p>
                                                </div>
                                                <div class="fallback">
                                                <input name="file" type="file" single /> 
                                                </div>
                                </form> 
                                <div class="form-group save-changes">
                                <a href="" class="btn btn-black"> Save Changes</a>
                                </div>
                                
                            </div>
                            
                            </div>
                </div>
                
            </div>
        </div>

        <div id="costumModalEditProfile" class="modal modal-auth" data-easein="slideUpBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                                <div class="heading-text">
                                    <h3><strong>Edit</strong> Your Profile</h3>
                                    <p>Edit your personal info inorder to update</p>
                                </div>
                            </div>
                            <div class="modal-body">
                            <form action="" class="form-wrapper">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Edit First Name</label>
                                            <input type="text" class="form-control" placeholder="Ashish" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="">Edit Last Name</label>
                                            <input type="text" class="form-control" placeholder="Dhakal" autofocus>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Edit Your Location</label>
                                    <input type="text" class="form-control" placeholder="Kathmandu , Nepal" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="">Edit Your Description</label>
                                    <textarea name="" id="" cols="30" rows="4" class="form-control">I am a passionate photographer always looking for new things to discover around us and analyze to style them through photograph</textarea>
                                </div>
                                <div class="form-group">
                                    <a href="" class="btn btn-black">Save Changes</a>
                                </div>
                            </form>
                            <div class="well well-sm">
                                <p>Looking to change password . <a href="" data-toggle="modal" data-target="#costumModalChangePassword" role="button" data-dismiss="modal" >Click Here</a></p>
                            </div>
                            
                            </div>
                </div>
                
            </div>
        </div>

        <div id="costumModalChangePassword" class="modal modal-auth" data-easein="slideDownBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                                <div class="heading-text">
                                    <h3><strong>Change</strong> Your Password</h3>
                                    <p>Edit your personal info inorder to update</p>
                                </div>
                            </div>
                            <div class="modal-body">
                            <form action="" class="form-wrapper">
                                        <div class="form-group">
                                            <ul class="list-inline">
                                                <li><label for="">Enter Your Old Password</label></li>
                                                <li class="pull-right"><label for=""><a href="">Forgot Password ? </a></label></li>
                                            </ul>
                                            <input type="password" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Create New Password</label>
                                            <input type="password" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Re-Enter New Password</label>
                                            <input type="password" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <a href="" class="btn btn-black">Change Password</a>
                                        </div>
                            </form>
                            <div class="well well-sm">
                                <p>Looking to change more info . <a href="" data-toggle="modal" data-target="#costumModalEditProfile" role="button" data-dismiss="modal" >Click Here</a></p>
                            </div>
                            
                            </div>
                </div>
                
            </div>
        </div>

        <script>
            $('.modal-search-slider').slick({
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows:true,
                autoplay: true,
                autoplaySpeed: 1000
                });
        </script>