<nav class="navbar navbar-default custom-navbar navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="assets/images/png/logo.png" alt="360 Media Distribution"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="">360 Images</a></li>
            <li><a href="">360 Videos</a></li>
            <li><a href="" data-toggle="modal" data-target="#costumModalSignIn">Sign In</a></li>
            <li><a href="" data-toggle="modal" data-target="#costumModalSearch"><span class=""><img src="assets/svg/search-icon.svg" alt="search-360"></span></a></li>
            <li><a href="upload.php"><span class=""><img src="assets/svg/upload-arrow.svg" alt="upload-360"></span></a></li>
          </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>