 <div class="side-tab-wrapper">

                    <div class="side-tab" id="categories">
                        <div class="side-tab-head">
                            <div class="side-tab-head-wrapper">
                                <h4><span><img src="assets/svg/categories.svg" alt=""></span> Categories</h4>
                            </div>
                        </div>
                        <div class="side-tab-body">
                            <div class="side-tab-body-wrapper">
                                <ul class="list-unstyled">
                                    <li><span class="badge pull-right">10</span><a href="travel-template.php">Travel & Tours</a></li>
                                    <li><span class="badge pull-right">12</span><a href="travel-template.php">Foods</a></li>
                                    <li><span class="badge pull-right">22</span><a href="travel-template.php">Culture & Society</a></li>
                                    <li><span class="badge pull-right">08</span><a href="travel-template.php">Infrastructure</a></li>
                                    <li><span class="badge pull-right">32</span><a href="travel-template.php">Fashions</a></li>
                                    <li><span class="badge pull-right">11</span><a href="travel-template.php">Sports & Action</a></li>
                                    <li><span class="badge pull-right">23</span><a href="travel-template.php">Events & Ocassions</a></li>
                                    <li><span class="badge pull-right">03</span><a href="travel-template.php">Animated</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="side-tab-footer">
                            <div class="side-tab-footer-wrapper">
                                <a href="">See All Media <span><img src="assets/svg/right-arrow.svg" alt=""></span></a>
                            </div>
                        </div>
                    </div>

                    <div class="side-tab" id="latest">
                        <div class="side-tab-head">
                            <div class="side-tab-head-wrapper">
                                <h4><span><img src="assets/svg/cam-icon.svg" alt=""></span> Latest Videos</h4>
                            </div>
                        </div>
                        <div class="side-tab-body">
                            <div class="side-tab-body-wrapper">
                                <ul class="list-unstyled">
                                    <li><a href="media-player.php">Eco Tourism</a></li>
                                    <li><a href="media-player.php">Jungle safari is heaven</a></li>
                                    <li><a href="media-player.php">Mountain peak is best</a></li>
                                    <li><a href="media-player.php">Rally through bridge</a></li>
                                    <li><a href="media-player.php">Heaven in earth</a></li>
                                    <li><a href="media-player.php">Sailing through cave</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="side-tab-footer">
                            <div class="side-tab-footer-wrapper">
                                <a href="">See All Latest <span><img src="assets/svg/right-arrow.svg" alt=""></span></a>
                            </div>
                        </div>
                    </div>

                    <div class="side-tab" id="facebook-iframe">
                        <div class="fb-page" data-href="https://www.facebook.com/paracosmainc/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true" data-width="372px">
                            <blockquote cite="https://www.facebook.com/paracosmainc/" class="fb-xfbml-parse-ignore">
                                <a href="https://www.facebook.com/paracosmainc/">Paracosma</a>
                            </blockquote>
                        </div>
                    </div>

                    <div class="side-tab" id="youtube-tab">
                        <ul class="list-inline text-center">
                            <li><span class="com-img"><img src="assets/images/png/paracosma-youtube.png" alt=""></span></li>
                            <li class="text-left desc"><strong>Paracosma</strong> <br> <small> 47 Subscriber</small></li>
                        </ul>
                        <div class="text-center">
                            <a href="" class="btn btn-ytd">SUSCRIBE US ON YOUTUBE</a>
                        </div>
                    </div>

                </div>