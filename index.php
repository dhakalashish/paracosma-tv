<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>

<section class="main-banner">
    <div class="main-banner-wrapper">
        <div class="banner-image-wrapper">
            <figure>
                <img src="assets/images/main@banner-2.jpg" alt="360 media platform" class="img-responsive">
            </figure>
        </div>
        <div class="banner-image-author">
            <div class="container-fluid">
                <ul class='list-inline'>
                    <li class="auth-image-list">
                        <a href="">
                        <img src="assets/images/author/auth-7.jpg" alt="" class="auth-img">
                        </a>
                    </li>
                    <li class="auth-name-list">
                        <strong>By</strong><br>
                        <span>Jessy Jane</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <?php include 'includes/side-tabs.php';?>
            </div>
            <div class="col-md-10">
                <div class="content-wrapper">
                    <div class="featured-content">
                        <div class="content-headline">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4><span> <img src="assets/svg/featured.svg" alt=""></span> Featured Media</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="active"><a data-toggle="tab" href="#all">All</a></li>
                                        <li><a data-toggle="tab" href="#images">360 Images</a></li>
                                        <li><a data-toggle="tab" href="#videos">360 Videos</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">
                                <div id="all" class="tab-pane fade in active">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-3.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-4.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-5.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-6.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="images" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="videos" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/11.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/8.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/15.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/10.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/9.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>

                    <div class="latest-content">
                        <div class="content-headline">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4><span> <img src="assets/svg/latest.svg" alt=""></span> Latest Media</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="active"><a data-toggle="tab" href="#latest-all">All</a></li>
                                        <li><a data-toggle="tab" href="#latest-images">360 Images</a></li>
                                        <li><a data-toggle="tab" href="#latest-videos">360 Videos</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">
                                <div id="latest-all" class="tab-pane fade in active">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/8.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="latest-images" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/15.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/10.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="latest-videos" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/11.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/8.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>

                    <div class="Most-view-content">
                        <div class="content-headline">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4><span> <img src="assets/svg/heart.svg" alt=""></span> Most Viewed Media</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="active"><a data-toggle="tab" href="#most-all">All</a></li>
                                        <li><a data-toggle="tab" href="#most-images">360 Images</a></li>
                                        <li><a data-toggle="tab" href="#most-videos">360 Videos</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="content-body">
                            <div class="tab-content">
                                <div id="most-all" class="tab-pane fade in active">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-3.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-4.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-5.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-6.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="most-images" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/6.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/3.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/2.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/4.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/5.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/travel-1.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="most-videos" class="tab-pane fade">
                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/12.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/11.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/8.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box">
                                                    <div class="content-box">
                                                        <figure>
                                                            <img src="assets/images/15.jpg" alt="" class="img-responsive">
                                                        </figure>
                                                        <div class="content-media">
                                                            <h4>Sunset with the berry of bim</h4>
                                                            <div class="media-detail">
                                                                <ul class="list-inline">
                                                                    <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                    <li>Nikki</li>
                                                                    <li class="border-pane"> 10 weeks ago</li>

                                                                    <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                </ul>
                                                            </div>
                                                            <div class="media-ancor">
                                                                <a href="media-player.php"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/10.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="box">
                                                        <div class="content-box">
                                                            <figure>
                                                                <img src="assets/images/9.jpg" alt="" class="img-responsive">
                                                            </figure>
                                                            <div class="content-media">
                                                                <h4>Sunset with the berry of bim</h4>
                                                                <div class="media-detail">
                                                                    <ul class="list-inline">
                                                                        <li><img src="assets/images/author/auth-2.jpg" alt="" class="auth-img img-responsive"></li>
                                                                        <li>Nikki</li>
                                                                        <li class="border-pane"> 10 weeks ago</li>

                                                                        <li class="pull-right"><span><img src="assets/svg/comments.svg" alt=""></span> 11</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="media-ancor">
                                                                    <a href="media-player.php"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        

<?php include 'includes/footer.php';?>
<?php include 'includes/modals.php';?>

        